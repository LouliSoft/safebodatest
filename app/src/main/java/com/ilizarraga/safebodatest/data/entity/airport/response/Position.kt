package com.ilizarraga.safebodatest.data.entity.airport.response

import com.google.gson.annotations.SerializedName

class Position(@SerializedName("Coordinate") val coordinate: Coordinate)