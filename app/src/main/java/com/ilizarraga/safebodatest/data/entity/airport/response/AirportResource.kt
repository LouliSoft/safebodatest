package com.ilizarraga.safebodatest.data.entity.airport.response

import com.google.gson.annotations.SerializedName

class AirportResource(@SerializedName("Airports") val airports: Airports)