package com.ilizarraga.safebodatest.data.net

import com.ilizarraga.safebodatest.BuildConfig
import com.ilizarraga.safebodatest.data.net.interceptor.HttpInterceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


class ApiConnection {

    companion object {

        val instance: RestApi by lazy {
            val retrofit: Retrofit =  Retrofit.Builder()
                    .baseUrl(BuildConfig.BASE_URL)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(getClient())
                    .build()
            retrofit.create(RestApi::class.java)
        }

        private fun getClient() : OkHttpClient {
            val builderClientHttp: OkHttpClient.Builder = OkHttpClient().newBuilder()
            builderClientHttp.connectTimeout(10, TimeUnit.SECONDS)
                    .writeTimeout(10, TimeUnit.SECONDS)
                    .readTimeout(10, TimeUnit.SECONDS)
                    .interceptors().add(HttpInterceptor())
            return builderClientHttp.build()
        }
    }
}