package com.ilizarraga.safebodatest.data.entity.schedule.mapper

import com.ilizarraga.safebodatest.data.entity.schedule.response.Flight
import com.ilizarraga.safebodatest.data.entity.schedule.response.Schedule
import com.ilizarraga.safebodatest.data.entity.schedule.response.ScheduleResponse
import com.ilizarraga.safebodatest.domain.entity.FlightEntity
import com.ilizarraga.safebodatest.domain.entity.ScheduleEntity

class ScheduleDataMapper {

    fun transform (scheduleResponse: ScheduleResponse) : List<ScheduleEntity> {
        val scheduleList: ArrayList<ScheduleEntity> = ArrayList()
        for (scheduleData:Schedule in scheduleResponse.scheduleResource.scheduleList) {
            val flightList: ArrayList<FlightEntity> = ArrayList()
            for (flightData: Flight in scheduleData.flight) {
                val flightRepository = FlightEntity(
                        flightData.departure.scheduledTimeLocal.dateTime,
                        flightData.arrival.scheduledTimeLocal.dateTime,
                        flightData.marketingCarrier.airlineID + flightData.marketingCarrier.flightNumber)
                flightList.add(flightRepository)
            }
            scheduleList.add(ScheduleEntity(flightList))
        }
        return scheduleList
    }
}