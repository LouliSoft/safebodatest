package com.ilizarraga.safebodatest.data.entity.schedule.response

import com.google.gson.annotations.SerializedName

class Flight(@SerializedName("Departure") val departure: Departure, @SerializedName("Arrival") val arrival: Arrival, @SerializedName("MarketingCarrier") val marketingCarrier: MarketingCarrier)