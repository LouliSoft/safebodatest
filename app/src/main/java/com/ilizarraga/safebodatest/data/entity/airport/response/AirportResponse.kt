package com.ilizarraga.safebodatest.data.entity.airport.response

import com.google.gson.annotations.SerializedName

class AirportResponse(@SerializedName("AirportResource") val airportResource: AirportResource)