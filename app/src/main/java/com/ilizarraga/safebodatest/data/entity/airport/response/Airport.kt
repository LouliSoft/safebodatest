package com.ilizarraga.safebodatest.data.entity.airport.response

import com.google.gson.annotations.SerializedName

class Airport(@SerializedName("AirportCode") val airportCode: String, @SerializedName("Position") val position: Position, @SerializedName("CityCode") val cityCode: String, @SerializedName("CountryCode") val countryCode: String, @SerializedName("Names") val names: Names)