package com.ilizarraga.safebodatest.data.repository

import com.ilizarraga.safebodatest.BuildConfig
import com.ilizarraga.safebodatest.data.constants.DataConstants
import com.ilizarraga.safebodatest.data.entity.schedule.mapper.ScheduleDataMapper
import com.ilizarraga.safebodatest.data.net.RestApi
import com.ilizarraga.safebodatest.data.net.RestApiImpl
import com.ilizarraga.safebodatest.domain.entity.ScheduleEntity
import com.ilizarraga.safebodatest.domain.repository.ScheduleRepository
import io.reactivex.Single

class ScheduleRepositoryImpl : ScheduleRepository {

    private var restApi: RestApi = RestApiImpl()
    private val repositoriesDataMapper: ScheduleDataMapper = ScheduleDataMapper()

    override fun getScheduleList(origin: String, destination: String, fromDateTime: String, offset: String, directFlights: Boolean) : Single<List<ScheduleEntity>> {
        return restApi.getToken(BuildConfig.CLIENT_ID, BuildConfig.CLIENT_SECRET, BuildConfig.GRANT_TYPE)
                .flatMap {
                    tokenRequest ->
                    restApi.getSchedule(DataConstants.BEARER + tokenRequest.accessToken, origin, destination, fromDateTime, offset, directFlights)
                        .map { repositoriesDataMapper.transform(it) }
                }
    }
}