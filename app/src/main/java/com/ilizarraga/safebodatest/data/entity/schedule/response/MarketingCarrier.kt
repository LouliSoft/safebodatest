package com.ilizarraga.safebodatest.data.entity.schedule.response

import com.google.gson.annotations.SerializedName

class MarketingCarrier(@SerializedName("AirlineID") val airlineID: String, @SerializedName("FlightNumber") val flightNumber: Int)