package com.ilizarraga.safebodatest.data.entity.airport.response

import com.google.gson.annotations.SerializedName

class Name(@SerializedName("$") val name: String)