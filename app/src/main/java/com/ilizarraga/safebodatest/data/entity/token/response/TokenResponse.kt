package com.ilizarraga.safebodatest.data.entity.token.response

import com.google.gson.annotations.SerializedName

class TokenResponse(@SerializedName("access_token") val accessToken: String)