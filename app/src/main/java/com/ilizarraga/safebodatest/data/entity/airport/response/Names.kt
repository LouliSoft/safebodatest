package com.ilizarraga.safebodatest.data.entity.airport.response

import com.google.gson.annotations.SerializedName

class Names(@SerializedName("Name") val name: Name)