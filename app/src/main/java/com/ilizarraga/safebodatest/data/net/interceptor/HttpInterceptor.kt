package com.ilizarraga.safebodatest.data.net.interceptor

import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response


class HttpInterceptor : Interceptor {

    private val syncObj = Any()

    override fun intercept(chain: Interceptor.Chain?): Response {
        synchronized(syncObj) {
            val builder: Request.Builder = chain!!.request().newBuilder()
            builder.addHeader("Accept", "application/json")
            return chain.proceed(builder.build())
        }
    }
}