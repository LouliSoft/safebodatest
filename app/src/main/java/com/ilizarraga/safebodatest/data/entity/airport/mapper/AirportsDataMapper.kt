package com.ilizarraga.safebodatest.data.entity.airport.mapper

import com.ilizarraga.safebodatest.data.entity.airport.response.Airport
import com.ilizarraga.safebodatest.data.entity.airport.response.AirportResponse
import com.ilizarraga.safebodatest.domain.entity.AirportEntity

class AirportsDataMapper {

    fun transform (airportResponse: AirportResponse) : List<AirportEntity> {
        val airportsList: ArrayList<AirportEntity> = ArrayList()
        for (airportData: Airport in airportResponse.airportResource.airports.airportList) {
            val repository = AirportEntity(
                    airportData.airportCode,
                    airportData.cityCode,
                    airportData.countryCode,
                    airportData.names.name.name,
                    airportData.position.coordinate.latitude,
                    airportData.position.coordinate.longitude)
            airportsList.add(repository)
        }
        return airportsList
    }
}