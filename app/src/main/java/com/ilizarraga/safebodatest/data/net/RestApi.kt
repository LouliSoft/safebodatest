package com.ilizarraga.safebodatest.data.net

import com.ilizarraga.safebodatest.data.entity.airport.response.AirportResponse
import com.ilizarraga.safebodatest.data.entity.schedule.response.ScheduleResponse
import com.ilizarraga.safebodatest.data.entity.token.response.TokenResponse
import io.reactivex.Single
import retrofit2.http.*

interface RestApi {

    @FormUrlEncoded
    @Headers("Content-Type: application/x-www-form-urlencoded")
    @POST("oauth/token")
    fun getToken(@Field("client_id") clientId: String, @Field("client_secret") clientSecret: String, @Field("grant_type") grantType: String) : Single<TokenResponse>

    @GET("references/airports")
    fun getAirports(@Header("Authorization") authorization: String, @Query("offset") offset: String, @Query("lang") lang: String, @Query("LHoperated") lHoperated: Boolean) : Single<AirportResponse>

    @GET("operations/schedules/{origin}/{destination}/{fromDateTime}")
    fun getSchedule(@Header("Authorization") authorization: String, @Path("origin") origin: String, @Path("destination") destination: String, @Path("fromDateTime") fromDateTime: String, @Query("offset") offset: String, @Query("directFlights") directFlights: Boolean) : Single<ScheduleResponse>

}