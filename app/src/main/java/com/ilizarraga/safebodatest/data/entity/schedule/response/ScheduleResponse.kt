package com.ilizarraga.safebodatest.data.entity.schedule.response

import com.google.gson.annotations.SerializedName

class ScheduleResponse(@SerializedName("ScheduleResource") val scheduleResource: ScheduleResource)