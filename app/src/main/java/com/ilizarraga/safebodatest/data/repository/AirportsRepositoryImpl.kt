package com.ilizarraga.safebodatest.data.repository

import com.ilizarraga.safebodatest.BuildConfig
import com.ilizarraga.safebodatest.data.constants.DataConstants
import com.ilizarraga.safebodatest.data.entity.airport.mapper.AirportsDataMapper
import com.ilizarraga.safebodatest.data.net.RestApi
import com.ilizarraga.safebodatest.data.net.RestApiImpl
import com.ilizarraga.safebodatest.domain.entity.AirportEntity
import com.ilizarraga.safebodatest.domain.repository.AirportsRepository
import io.reactivex.Single

class AirportsRepositoryImpl : AirportsRepository {

    private var restApi: RestApi = RestApiImpl()
    private val repositoriesDataMapper: AirportsDataMapper = AirportsDataMapper()

    override fun getAirportsList(offset: String, lang: String, lHoperated: Boolean) : Single<List<AirportEntity>> {
        return restApi.getToken(BuildConfig.CLIENT_ID, BuildConfig.CLIENT_SECRET, BuildConfig.GRANT_TYPE)
                .flatMap {
                    tokenRequest ->
                    restApi.getAirports(DataConstants.BEARER + tokenRequest.accessToken, offset, lang, lHoperated)
                        .map { repositoriesDataMapper.transform(it) }
                }
    }
}