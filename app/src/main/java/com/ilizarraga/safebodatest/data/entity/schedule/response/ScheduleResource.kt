package com.ilizarraga.safebodatest.data.entity.schedule.response

import com.google.gson.annotations.SerializedName

class ScheduleResource(@SerializedName("Schedule") val scheduleList: List<Schedule>)