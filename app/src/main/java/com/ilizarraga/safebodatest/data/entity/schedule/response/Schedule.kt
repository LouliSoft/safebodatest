package com.ilizarraga.safebodatest.data.entity.schedule.response

import com.google.gson.annotations.SerializedName

class Schedule(@SerializedName("Flight") val flight: List<Flight>)