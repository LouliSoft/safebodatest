package com.ilizarraga.safebodatest.data.entity.schedule.response

import com.google.gson.annotations.SerializedName

class Departure(@SerializedName("ScheduledTimeLocal") val scheduledTimeLocal: ScheduledTimeLocal)