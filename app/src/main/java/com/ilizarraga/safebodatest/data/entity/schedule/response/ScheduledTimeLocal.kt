package com.ilizarraga.safebodatest.data.entity.schedule.response

import com.google.gson.annotations.SerializedName

class ScheduledTimeLocal(@SerializedName("DateTime") val dateTime: String)