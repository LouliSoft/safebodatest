package com.ilizarraga.safebodatest.data.entity.airport.response

import com.google.gson.annotations.SerializedName

class Coordinate(@SerializedName("Latitude") val latitude: Double, @SerializedName("Longitude") val longitude: Double)