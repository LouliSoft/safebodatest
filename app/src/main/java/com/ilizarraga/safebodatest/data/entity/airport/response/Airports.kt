package com.ilizarraga.safebodatest.data.entity.airport.response

import com.google.gson.annotations.SerializedName

class Airports(@SerializedName("Airport") val airportList: List<Airport>)