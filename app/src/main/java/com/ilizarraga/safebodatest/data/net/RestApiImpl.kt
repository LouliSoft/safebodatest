package com.ilizarraga.safebodatest.data.net

import com.ilizarraga.safebodatest.data.entity.airport.response.AirportResponse
import com.ilizarraga.safebodatest.data.entity.schedule.response.ScheduleResponse
import com.ilizarraga.safebodatest.data.entity.token.response.TokenResponse
import io.reactivex.Single

class RestApiImpl : RestApi {

    private val apiConnection: RestApi = ApiConnection.instance

    override fun getToken(clientId: String, clientSecret: String, grantType: String) : Single<TokenResponse> {
        return apiConnection.getToken(clientId, clientSecret, grantType)
    }

    override fun getAirports(authorization: String, offset: String, lang: String, lHoperated: Boolean) : Single<AirportResponse> {
        return apiConnection.getAirports(authorization, offset, lang, lHoperated)
    }

    override fun getSchedule(authorization: String, origin: String, destination: String, fromDateTime: String, offset: String, directFlights: Boolean): Single<ScheduleResponse> {
        return apiConnection.getSchedule(authorization, origin, destination, fromDateTime, offset, directFlights)
    }
}