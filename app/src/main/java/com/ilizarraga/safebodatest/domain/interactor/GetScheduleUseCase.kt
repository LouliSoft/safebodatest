package com.ilizarraga.safebodatest.domain.interactor

import com.ilizarraga.safebodatest.domain.entity.ScheduleEntity
import com.ilizarraga.safebodatest.domain.repository.ScheduleRepository
import io.reactivex.Single

class GetScheduleUseCase(private val scheduleRepository: ScheduleRepository) {

    fun buildUseCaseObservable(origin: String, destination: String, fromDateTime: String, offset: String, directFlights: Boolean = false) : Single<List<ScheduleEntity>> {
        return scheduleRepository.getScheduleList(origin, destination, fromDateTime, offset, directFlights)
    }

}