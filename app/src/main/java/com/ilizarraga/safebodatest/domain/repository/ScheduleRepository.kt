package com.ilizarraga.safebodatest.domain.repository

import com.ilizarraga.safebodatest.domain.entity.ScheduleEntity
import io.reactivex.Single

interface ScheduleRepository {

    fun getScheduleList(origin: String, destination: String, fromDateTime: String, offset: String, directFlights: Boolean) : Single<List<ScheduleEntity>>

}