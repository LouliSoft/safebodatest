package com.ilizarraga.safebodatest.domain.repository

import com.ilizarraga.safebodatest.domain.entity.AirportEntity
import io.reactivex.Single

interface AirportsRepository {

    fun getAirportsList(offset: String, lang: String, lHoperated: Boolean) : Single<List<AirportEntity>>

}