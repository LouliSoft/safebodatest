package com.ilizarraga.safebodatest.domain.interactor

import com.ilizarraga.safebodatest.domain.entity.AirportEntity
import com.ilizarraga.safebodatest.domain.repository.AirportsRepository
import io.reactivex.Single

class GetAirportsUseCase(private val airportsRepository: AirportsRepository) {

    fun buildUseCaseObservable(offset: String, lang: String = "EN", lHoperated: Boolean = false) : Single<List<AirportEntity>> {
        return airportsRepository.getAirportsList(offset, lang, lHoperated)
    }

}