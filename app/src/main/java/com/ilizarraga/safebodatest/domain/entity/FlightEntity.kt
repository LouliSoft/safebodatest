package com.ilizarraga.safebodatest.domain.entity

class FlightEntity(val departure: String, val arrival: String, val airline: String)