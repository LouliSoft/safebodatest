package com.ilizarraga.safebodatest.domain.entity

class AirportEntity(val airportCode: String, val cityCode: String, val countryCode: String, val name: String, val latitude: Double, val longitude: Double)