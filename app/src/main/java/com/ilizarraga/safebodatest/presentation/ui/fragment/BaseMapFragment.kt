package com.ilizarraga.safebodatest.presentation.ui.fragment

import com.google.android.gms.maps.SupportMapFragment

open class BaseMapFragment : SupportMapFragment()