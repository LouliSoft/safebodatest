package com.ilizarraga.safebodatest.presentation.ui.view

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.util.AttributeSet
import com.ilizarraga.safebodatest.R
import com.ilizarraga.safebodatest.domain.entity.ScheduleEntity
import kotlinx.android.synthetic.main.view_holder_schedule.view.*

class ScheduleItemView : ConstraintLayout {

    constructor(context: Context) : super(context) {
        initialize()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        initialize()
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        initialize()
    }

    private fun initialize() {
        inflate(context, R.layout.view_holder_schedule, this)
    }

    fun decorate(scheduleEntity: ScheduleEntity) {
        var departure = ""
        var arrival = ""
        var airline = ""
        for (flightEntity in scheduleEntity.flightEntityList) {
            departure += flightEntity.departure + ", "
            arrival += flightEntity.arrival + ", "
            airline += flightEntity.airline + ", "
        }
        view_holder_schedule_departure_text.setText(R.string.departure)
        view_holder_schedule_departure_text_desc.text = if (departure.isEmpty()) "" else departure.trim().substring(0, departure.length - 2)
        view_holder_schedule_arrival_text.setText(R.string.arrival)
        view_holder_schedule_arrival_text_desc.text = if (arrival.isEmpty()) "" else arrival.trim().substring(0, arrival.length - 2)
        view_holder_schedule_airline_text.setText(R.string.airline)
        view_holder_schedule_airline_text_desc.text = if (airline.isEmpty()) "" else airline.trim().substring(0, airline.length - 2)
    }
}