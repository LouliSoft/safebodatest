package com.ilizarraga.safebodatest.presentation.ui.adapter.holder

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import com.ilizarraga.safebodatest.domain.entity.ScheduleEntity
import com.ilizarraga.safebodatest.presentation.ui.listener.OnScheduleCellClickListener
import com.ilizarraga.safebodatest.presentation.ui.view.ScheduleItemView



class ScheduleViewHolder(private val scheduleItemView: ScheduleItemView, private val onScheduleCellClickListener: OnScheduleCellClickListener) : RecyclerView.ViewHolder(scheduleItemView) {

    fun decorate(scheduleEntity: ScheduleEntity) {
        scheduleItemView.startAnimation(AnimationUtils.loadAnimation(scheduleItemView.context, android.R.anim.slide_in_left))
        scheduleItemView.layoutParams = RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        scheduleItemView.decorate(scheduleEntity)
        scheduleItemView.setOnClickListener { onScheduleCellClickListener.onCellClick(scheduleEntity) }
    }
}