package com.ilizarraga.safebodatest.presentation.presenter.contract

import com.ilizarraga.safebodatest.presentation.presenter.BasePresenter
import com.ilizarraga.safebodatest.presentation.presenter.BaseView

interface SplashContract {

    interface View : BaseView<Presenter>

    interface Presenter : BasePresenter

}