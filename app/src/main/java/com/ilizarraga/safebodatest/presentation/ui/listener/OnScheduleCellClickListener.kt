package com.ilizarraga.safebodatest.presentation.ui.listener

import com.ilizarraga.safebodatest.domain.entity.ScheduleEntity

interface OnScheduleCellClickListener {

    fun onCellClick(scheduleEntity: ScheduleEntity)

}