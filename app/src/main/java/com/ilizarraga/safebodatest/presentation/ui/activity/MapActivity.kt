package com.ilizarraga.safebodatest.presentation.ui.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.ilizarraga.safebodatest.R
import com.ilizarraga.safebodatest.presentation.ui.fragment.MapFragment

class MapActivity : BaseActivity() {

    companion object {

        const val NO_VALUE: Double = 0.0

        const val ORIGIN_LAT: String = "ORIGIN_LAT"
        const val ORIGIN_LONG: String = "ORIGIN_LONG"
        const val DESTINATION_LAT: String = "DESTINATION_LAT"
        const val DESTINATION_LONG: String = "DESTINATION_LONG"

        fun makeIntent(context: Context?, originLat: Double, originLong: Double, destinationLat: Double, destinationLong: Double): Intent {
            return Intent(context, MapActivity::class.java).putExtra(ORIGIN_LAT, originLat)
                    .putExtra(ORIGIN_LONG, originLong).putExtra(DESTINATION_LAT, destinationLat)
                    .putExtra(DESTINATION_LONG, destinationLong)
        }

    }

    private var mapFragment: MapFragment? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_base)

        val originLat: Double = intent.getDoubleExtra(ORIGIN_LAT, NO_VALUE)
        val originLong: Double = intent.getDoubleExtra(ORIGIN_LONG, NO_VALUE)
        val destinationLat: Double = intent.getDoubleExtra(DESTINATION_LAT, NO_VALUE)
        val destinationLong: Double = intent.getDoubleExtra(DESTINATION_LONG, NO_VALUE)

        mapFragment = supportFragmentManager.findFragmentById(R.id.base_container) as MapFragment?

        if (mapFragment == null) {
            mapFragment = MapFragment.newInstance(originLat, originLong, destinationLat, destinationLong)

            supportFragmentManager
                    .beginTransaction()
                    .add(R.id.base_container, mapFragment)
                    .commit()
        }
    }
}
