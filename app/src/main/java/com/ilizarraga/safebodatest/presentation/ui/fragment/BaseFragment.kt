package com.ilizarraga.safebodatest.presentation.ui.fragment

import android.support.v4.app.Fragment

open class BaseFragment : Fragment()