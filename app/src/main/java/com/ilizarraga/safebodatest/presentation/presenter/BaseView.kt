package com.ilizarraga.safebodatest.presentation.presenter

interface BaseView<T> {

    var mPresenter: T

}