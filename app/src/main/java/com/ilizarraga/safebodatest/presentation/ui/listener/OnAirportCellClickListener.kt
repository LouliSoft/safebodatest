package com.ilizarraga.safebodatest.presentation.ui.listener

import com.ilizarraga.safebodatest.domain.entity.AirportEntity

interface OnAirportCellClickListener {

    fun onCellClick(airportEntity: AirportEntity)

}