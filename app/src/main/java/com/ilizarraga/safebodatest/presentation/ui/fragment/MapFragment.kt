package com.ilizarraga.safebodatest.presentation.ui.fragment

import android.content.pm.PackageManager
import android.graphics.Color
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.View
import android.widget.Toast
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.model.PolylineOptions
import com.ilizarraga.safebodatest.presentation.presenter.MapPresenter
import com.ilizarraga.safebodatest.presentation.presenter.contract.MapContract

class MapFragment : BaseMapFragment(), MapContract.View, OnMapReadyCallback {

    companion object {

        const val NO_VALUE: Double = 0.0

        const val ORIGIN_LAT: String = "ORIGIN_LAT"
        const val ORIGIN_LONG: String = "ORIGIN_LONG"
        const val DESTINATION_LAT: String = "DESTINATION_LAT"
        const val DESTINATION_LONG: String = "DESTINATION_LONG"

        const val PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: Int = 303

        fun newInstance(originLat: Double, originLong: Double, destinationLat: Double, destinationLong: Double) : MapFragment {
            val mapFragment = MapFragment()
            val args = Bundle()
            args.putDouble(ORIGIN_LAT, originLat)
            args.putDouble(ORIGIN_LONG, originLong)
            args.putDouble(DESTINATION_LAT, destinationLat)
            args.putDouble(DESTINATION_LONG, destinationLong)
            mapFragment.arguments = args
            return mapFragment
        }
    }

    private lateinit var mMap: GoogleMap

    private lateinit var mOriginLatLong: LatLng
    private lateinit var mDestinationLatLong: LatLng

    override var mPresenter: MapContract.Presenter = MapPresenter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (ContextCompat.checkSelfPermission(context!!, android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Permission is not granted
            requestPermissions(arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION)
        } else {
            // Permission has already been granted
            getMapAsync(this)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (arguments != null) {
            mOriginLatLong = LatLng(arguments!!.getDouble(ORIGIN_LAT, NO_VALUE), arguments!!.getDouble(ORIGIN_LONG, NO_VALUE))
            mDestinationLatLong = LatLng(arguments!!.getDouble(DESTINATION_LAT, NO_VALUE), arguments!!.getDouble(DESTINATION_LONG, NO_VALUE))
        }

        mPresenter.start()
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        mMap.addMarker(MarkerOptions().position(mOriginLatLong))
        mMap.addMarker(MarkerOptions().position(mDestinationLatLong))

        mMap.addPolyline(PolylineOptions()
                .add(mOriginLatLong, mDestinationLatLong)
                .width(5f)
                .color(Color.RED))

        mMap.setOnMapLoadedCallback {
            val builder: LatLngBounds.Builder = LatLngBounds.Builder()
            builder.include(mOriginLatLong)
            builder.include(mDestinationLatLong)
            mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), 0))
        }

    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {

        when (requestCode) {
            PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION -> {

                if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(context, "Unable to show location - permission required", Toast.LENGTH_LONG).show()
                } else {
                    getMapAsync(this)
                }
            }
        }
    }
}