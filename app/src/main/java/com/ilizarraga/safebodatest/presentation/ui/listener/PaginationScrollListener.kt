package com.ilizarraga.safebodatest.presentation.ui.listener

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView

abstract class PaginationScrollListener(private val linearLayoutManager : LinearLayoutManager) : RecyclerView.OnScrollListener() {

    override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)

        val totalItemCount : Int = linearLayoutManager.itemCount
        val visibleItems : Int = linearLayoutManager.childCount
        val firstItemPos : Int = linearLayoutManager.findFirstVisibleItemPosition()

        if (!isLastPage() && !isLoading()) {
            if ((visibleItems + firstItemPos) >= totalItemCount
                    && firstItemPos >= 0) {
                loadNextItems()
            }
        }
    }

    abstract fun isLoading() : Boolean

    abstract fun isLastPage() : Boolean

    abstract fun loadNextItems()
}