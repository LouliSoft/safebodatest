package com.ilizarraga.safebodatest.presentation.ui.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.ilizarraga.safebodatest.R
import com.ilizarraga.safebodatest.presentation.ui.fragment.ScheduleSelectorFragment

class ScheduleSelectorActivity : BaseActivity() {

    companion object {

        const val NO_VALUE: Double = 0.0

        const val ORIGIN: String = "ORIGIN"
        const val DESTINATION: String = "DESTINATION"
        const val ORIGIN_LAT: String = "ORIGIN_LAT"
        const val ORIGIN_LONG: String = "ORIGIN_LONG"
        const val DESTINATION_LAT: String = "DESTINATION_LAT"
        const val DESTINATION_LONG: String = "DESTINATION_LONG"

        fun makeIntent(context: Context?, origin: String, destination: String, originLat: Double, originLong: Double, destinationLat: Double, destinationLong: Double): Intent {
            return Intent(context, ScheduleSelectorActivity::class.java).putExtra(ORIGIN, origin)
                    .putExtra(DESTINATION, destination).putExtra(ORIGIN_LAT, originLat)
                    .putExtra(ORIGIN_LONG, originLong).putExtra(DESTINATION_LAT, destinationLat)
                    .putExtra(DESTINATION_LONG, destinationLong)
        }

    }

    private var scheduleSelectorFragment: ScheduleSelectorFragment? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_base)

        val origin: String = intent.getStringExtra(ORIGIN)
        val destination: String = intent.getStringExtra(DESTINATION)
        val originLat: Double = intent.getDoubleExtra(ORIGIN_LAT, NO_VALUE)
        val originLong: Double = intent.getDoubleExtra(ORIGIN_LONG, NO_VALUE)
        val destinationLat: Double = intent.getDoubleExtra(DESTINATION_LAT, NO_VALUE)
        val destinationLong: Double = intent.getDoubleExtra(DESTINATION_LONG, NO_VALUE)

        scheduleSelectorFragment = supportFragmentManager.findFragmentById(R.id.base_container) as ScheduleSelectorFragment?

        if (scheduleSelectorFragment == null) {
            scheduleSelectorFragment = ScheduleSelectorFragment.newInstance(origin, destination, originLat, originLong, destinationLat, destinationLong)

            supportFragmentManager
                    .beginTransaction()
                    .add(R.id.base_container, scheduleSelectorFragment)
                    .commit()
        }
    }
}