package com.ilizarraga.safebodatest.presentation.ui.fragment

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.ilizarraga.safebodatest.R
import com.ilizarraga.safebodatest.data.repository.AirportsRepositoryImpl
import com.ilizarraga.safebodatest.domain.entity.AirportEntity
import com.ilizarraga.safebodatest.presentation.presenter.AirportSelectorPresenter
import com.ilizarraga.safebodatest.presentation.presenter.contract.AirportSelectorContract
import com.ilizarraga.safebodatest.presentation.ui.activity.ScheduleSelectorActivity
import com.ilizarraga.safebodatest.presentation.ui.adapter.AirportListAdapter
import com.ilizarraga.safebodatest.presentation.ui.listener.OnAirportCellClickListener
import com.ilizarraga.safebodatest.presentation.ui.listener.PaginationScrollListener
import kotlinx.android.synthetic.main.fragment_airport_selector.*

class AirportSelectorFragment : BaseFragment(), AirportSelectorContract.View {

    companion object {

        val TAG: String = AirportSelectorFragment::class.java.simpleName

        fun newInstance() : AirportSelectorFragment {
            return AirportSelectorFragment()
        }
    }

    private var mOriginAirportCode : String? = null

    private var mIsLoading : Boolean = false
    private var mIsLastPage : Boolean = false

    private var mOriginLat: Double = 0.0
    private var mOriginLong: Double = 0.0

    private lateinit var mAdapter: AirportListAdapter

    override var mPresenter: AirportSelectorContract.Presenter = AirportSelectorPresenter(this, AirportsRepositoryImpl())

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_airport_selector, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        fragment_airport_selector_origin_text.setText(R.string.selectior_origin)
        fragment_airport_selector_destination_text.setText(R.string.selectior_destination)

        initRecycleView()
        mPresenter.start()
    }

    override fun showAirportEntityList(airportEntityList: List<AirportEntity>) = updateList(airportEntityList)

    override fun showTimeout() {
        Toast.makeText(context, "Server timeout, connecting again ", Toast.LENGTH_SHORT).show()
    }

    override fun showError(error: Throwable) {
        Log.e(TAG, error.message)
        Toast.makeText(context, "Server error: " + error.message, Toast.LENGTH_LONG).show()
    }

    private fun initRecycleView() {
        val linearLayoutManager = LinearLayoutManager(activity)
        fragment_airport_selector_rv.layoutManager = linearLayoutManager

        mAdapter = AirportListAdapter(object : OnAirportCellClickListener {
            override fun onCellClick(airportEntity: AirportEntity) {
                if (mOriginAirportCode == null) {
                    mOriginAirportCode = airportEntity.airportCode
                    mOriginLat = airportEntity.latitude
                    mOriginLong = airportEntity.longitude
                    fragment_airport_selector_origin_text_desc.text = airportEntity.name
                    fragment_airport_selector_rv.scrollToPosition(0)
                } else {
                    fragment_airport_selector_destination_text_desc.text = airportEntity.name
                    startActivity(ScheduleSelectorActivity.makeIntent(context,
                            mOriginAirportCode!!,
                            airportEntity.airportCode,
                            mOriginLat,
                            mOriginLong,
                            airportEntity.latitude,
                            airportEntity.longitude))
                }
            }
        })
        fragment_airport_selector_rv.adapter = mAdapter

        fragment_airport_selector_rv.addOnScrollListener(object : PaginationScrollListener(linearLayoutManager) {

            override fun isLoading() = mIsLoading

            override fun isLastPage() = mIsLastPage

            override fun loadNextItems() {
                showLoading()
                getNextPage()
            }

        })
    }

    private fun updateList(airportEntityList: List<AirportEntity>) {
        mAdapter.addAirportEntityList(airportEntityList)
        fragment_airport_selector_rv.adapter.notifyItemRangeInserted(mAdapter.itemCount, airportEntityList.size)
        hideLoading()
    }

    private fun getNextPage() = mPresenter.getAirportEntityList(mAdapter.itemCount.toString())

    private fun showLoading() {
        fragment_airport_selector_progressbar.visibility = View.VISIBLE
        mIsLoading = true
    }

    private fun hideLoading() {
        fragment_airport_selector_progressbar.visibility = View.GONE
        mIsLoading = false
    }

    override fun onPause() {
        super.onPause()
        mPresenter.clearObservables()
    }

    override fun onDestroy() {
        super.onDestroy()
        mPresenter.clearObservables()
    }
}