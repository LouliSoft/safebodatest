package com.ilizarraga.safebodatest.presentation.ui.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.ilizarraga.safebodatest.R
import com.ilizarraga.safebodatest.presentation.ui.fragment.AirportSelectorFragment

class AirportSelectorActivity : BaseActivity() {

    companion object {

        fun makeIntent(context: Context?): Intent {
            return Intent(context, AirportSelectorActivity::class.java)
        }

    }

    private var airportSelectorFragment: AirportSelectorFragment? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_base)

        airportSelectorFragment = supportFragmentManager.findFragmentById(R.id.base_container) as AirportSelectorFragment?

        if (airportSelectorFragment == null) {
            airportSelectorFragment = AirportSelectorFragment.newInstance()

            supportFragmentManager
                    .beginTransaction()
                    .add(R.id.base_container, airportSelectorFragment)
                    .commit()
        }
    }
}