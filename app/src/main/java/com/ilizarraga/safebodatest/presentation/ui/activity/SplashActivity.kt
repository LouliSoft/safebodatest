package com.ilizarraga.safebodatest.presentation.ui.activity

import android.annotation.TargetApi
import android.os.Build
import android.os.Bundle
import android.view.View
import com.ilizarraga.safebodatest.R
import com.ilizarraga.safebodatest.presentation.ui.fragment.SplashFragment



class SplashActivity : BaseActivity() {

    private var splashFragment: SplashFragment? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_base)

        splashFragment = supportFragmentManager.findFragmentById(R.id.base_container) as SplashFragment?

        if (splashFragment == null) {
            splashFragment = SplashFragment.newInstance()

            supportFragmentManager
                    .beginTransaction()
                    .add(R.id.base_container, splashFragment)
                    .commit()
        }
    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
        initializeWindow()
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        initializeWindow()
    }


    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        initializeWindow()
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    private fun initializeWindow() {
        window.decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION or View.SYSTEM_UI_FLAG_FULLSCREEN or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY)
    }

}