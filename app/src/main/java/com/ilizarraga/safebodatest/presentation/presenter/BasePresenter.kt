package com.ilizarraga.safebodatest.presentation.presenter

import io.reactivex.disposables.CompositeDisposable

interface BasePresenter {

    companion object {
        var disposables: CompositeDisposable = CompositeDisposable()
    }

    fun start()

    fun clearObservables() {
        // Using clear will clear all, but can accept new disposable
        disposables.clear()
    }

    fun disposeObservables() {
        // Using dispose will clear all and set isDisposed = true, so it will not accept any new disposable
        if(!disposables.isDisposed){
            disposables.dispose()
        }
    }
}