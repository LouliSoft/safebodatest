package com.ilizarraga.safebodatest.presentation.presenter

import com.ilizarraga.safebodatest.domain.entity.AirportEntity
import com.ilizarraga.safebodatest.domain.interactor.GetAirportsUseCase
import com.ilizarraga.safebodatest.domain.repository.AirportsRepository
import com.ilizarraga.safebodatest.presentation.presenter.contract.AirportSelectorContract
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import java.net.SocketTimeoutException

open class AirportSelectorPresenter(private val view: AirportSelectorContract.View, airportsRepository: AirportsRepository) : AirportSelectorContract.Presenter {

    companion object {
        const val NO_OFFSET: String = "0"
    }

    private var getAirportsUseCase: GetAirportsUseCase = GetAirportsUseCase(airportsRepository)
    lateinit var observable: Single<List<AirportEntity>>

    override fun start() {
        getAirportEntityList(NO_OFFSET)
    }

    override fun getAirportEntityList(offset: String) {
        observable = execute(offset)
        doSubscribe()
    }

    private fun execute(offset: String): Single<List<AirportEntity>> {
        return getAirportsUseCase.buildUseCaseObservable(offset)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    private fun doSubscribe() {
        observable.subscribe({ result -> onSuccess(result) }, { error -> onError(error) }).addTo(BasePresenter.disposables)
    }

    private fun onSuccess(airportEntityList: List<AirportEntity>) {
        view.showAirportEntityList(airportEntityList)
    }

    private fun onError(error: Throwable) {
        if (error is SocketTimeoutException) {
            view.showTimeout()
            doSubscribe()
        } else {
            view.showError(error)
        }
    }
}