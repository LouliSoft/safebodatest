package com.ilizarraga.safebodatest.presentation.presenter

import com.ilizarraga.safebodatest.domain.entity.ScheduleEntity
import com.ilizarraga.safebodatest.domain.interactor.GetScheduleUseCase
import com.ilizarraga.safebodatest.domain.repository.ScheduleRepository
import com.ilizarraga.safebodatest.presentation.presenter.contract.ScheduleSelectorContract
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import java.net.SocketTimeoutException

class ScheduleSelectorPresenter(private val view: ScheduleSelectorContract.View, scheduleRepository: ScheduleRepository) : ScheduleSelectorContract.Presenter {

    private var getScheduleUseCase: GetScheduleUseCase = GetScheduleUseCase(scheduleRepository)
    lateinit var observable: Single<List<ScheduleEntity>>

    override fun start() {

    }

    override fun getScheduleEntityList(origin: String, destination: String, fromDateTime: String, offset: String) {
        observable = execute(origin, destination, fromDateTime, offset)
        doSubscribe()
    }

    private fun execute(origin: String, destination: String, fromDateTime: String, offset: String) : Single<List<ScheduleEntity>> {
        return getScheduleUseCase.buildUseCaseObservable(origin, destination, fromDateTime, offset)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    private fun doSubscribe() {
        observable.subscribe({ result -> onSuccess(result) }, { error -> onError(error) }).addTo(BasePresenter.disposables)
    }

    private fun onSuccess(airportEntityList: List<ScheduleEntity>) {
        view.showScheduleEntityList(airportEntityList)
    }

    private fun onError(error: Throwable) {
        if (error is SocketTimeoutException) {
            view.showTimeout()
            doSubscribe()
        } else {
            view.showError(error)
        }
    }
}