package com.ilizarraga.safebodatest.presentation.presenter.contract

import com.ilizarraga.safebodatest.domain.entity.AirportEntity
import com.ilizarraga.safebodatest.presentation.presenter.BasePresenter
import com.ilizarraga.safebodatest.presentation.presenter.BaseView

interface AirportSelectorContract {

    interface View : BaseView<Presenter> {

        fun showAirportEntityList(airportEntityList: List<AirportEntity>)

        fun showTimeout()

        fun showError(error: Throwable)

    }

    interface Presenter : BasePresenter {

        fun getAirportEntityList(offset: String)

    }

}