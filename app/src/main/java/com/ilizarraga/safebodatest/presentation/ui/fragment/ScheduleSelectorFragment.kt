package com.ilizarraga.safebodatest.presentation.ui.fragment

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.ilizarraga.safebodatest.R
import com.ilizarraga.safebodatest.data.repository.ScheduleRepositoryImpl
import com.ilizarraga.safebodatest.domain.entity.ScheduleEntity
import com.ilizarraga.safebodatest.presentation.presenter.ScheduleSelectorPresenter
import com.ilizarraga.safebodatest.presentation.presenter.contract.ScheduleSelectorContract
import com.ilizarraga.safebodatest.presentation.ui.activity.MapActivity
import com.ilizarraga.safebodatest.presentation.ui.adapter.ScheduleListAdapter
import com.ilizarraga.safebodatest.presentation.ui.listener.OnScheduleCellClickListener
import com.ilizarraga.safebodatest.presentation.ui.listener.PaginationScrollListener
import com.ilizarraga.safebodatest.presentation.utils.TimestampUtils
import kotlinx.android.synthetic.main.fragment_schedule_selector.*

class ScheduleSelectorFragment : BaseFragment(), ScheduleSelectorContract.View {

    companion object {

        val TAG: String = ScheduleSelectorFragment::class.java.simpleName

        const val NO_VALUE: Double = 0.0

        const val ORIGIN: String = "ORIGIN"
        const val DESTINATION: String = "DESTINATION"
        const val ORIGIN_LAT: String = "ORIGIN_LAT"
        const val ORIGIN_LONG: String = "ORIGIN_LONG"
        const val DESTINATION_LAT: String = "DESTINATION_LAT"
        const val DESTINATION_LONG: String = "DESTINATION_LONG"

        fun newInstance(origin: String, destination: String, originLat: Double, originLong: Double, destinationLat: Double, destinationLong: Double) : ScheduleSelectorFragment {
            val scheduleSelectorFragment = ScheduleSelectorFragment()
            val args = Bundle()
            args.putString(ORIGIN, origin)
            args.putString(DESTINATION, destination)
            args.putDouble(ORIGIN_LAT, originLat)
            args.putDouble(ORIGIN_LONG, originLong)
            args.putDouble(DESTINATION_LAT, destinationLat)
            args.putDouble(DESTINATION_LONG, destinationLong)
            scheduleSelectorFragment.arguments = args
            return scheduleSelectorFragment
        }
    }

    private var mOrigin: String? = null
    private var mDestination: String? = null
    private var mOriginLat: Double? = null
    private var mOriginLong: Double? = null
    private var mDestinationLat: Double? = null
    private var mDestinationLong: Double? = null

    private var mIsLoading : Boolean = false
    private var mIsLastPage : Boolean = false

    private lateinit var mAdapter: ScheduleListAdapter

    override var mPresenter: ScheduleSelectorContract.Presenter = ScheduleSelectorPresenter(this, ScheduleRepositoryImpl())

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_schedule_selector, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (arguments != null) {
            mOrigin = arguments!!.getString(ORIGIN)
            mDestination = arguments!!.getString(DESTINATION)
            mOriginLat = arguments!!.getDouble(ORIGIN_LAT, NO_VALUE)
            mOriginLong = arguments!!.getDouble(ORIGIN_LONG, NO_VALUE)
            mDestinationLat = arguments!!.getDouble(DESTINATION_LAT, NO_VALUE)
            mDestinationLong = arguments!!.getDouble(DESTINATION_LONG, NO_VALUE)
        }

        initRecycleView()

        mPresenter.start()
    }

    override fun showScheduleEntityList(scheduleEntityList: List<ScheduleEntity>) = updateList(scheduleEntityList)

    override fun showTimeout() {
        Toast.makeText(context, "Server timeout, connecting again ", Toast.LENGTH_SHORT).show()
    }

    override fun showError(error: Throwable) {
        Log.e(ScheduleSelectorFragment.TAG, error.message)
        Toast.makeText(context, "Server error: " + error.message, Toast.LENGTH_LONG).show()
        activity!!.finish()
    }

    private fun initRecycleView() {
        val linearLayoutManager = LinearLayoutManager(activity)
        fragment_schedule_selector_rv.layoutManager = linearLayoutManager

        mAdapter = ScheduleListAdapter(object : OnScheduleCellClickListener {
            override fun onCellClick(scheduleEntity: ScheduleEntity) {
                startActivity(MapActivity.makeIntent(context, mOriginLat!!, mOriginLong!!, mDestinationLat!!, mDestinationLong!!))
            }
        })
        fragment_schedule_selector_rv.adapter = mAdapter

        fragment_schedule_selector_rv.addOnScrollListener(object : PaginationScrollListener(linearLayoutManager) {

            override fun isLoading() = mIsLoading

            override fun isLastPage() = mIsLastPage

            override fun loadNextItems() {
                showLoading()
                getNextPage()
            }

        })

        getNextPage()
    }

    private fun updateList(scheduleEntityList: List<ScheduleEntity>) {
        mAdapter.addScheduleEntityList(scheduleEntityList)
        fragment_schedule_selector_rv.adapter.notifyItemRangeInserted(mAdapter.itemCount, scheduleEntityList.size)
        hideLoading()
    }

    private fun getNextPage() = mPresenter.getScheduleEntityList(mOrigin!!, mDestination!!, TimestampUtils.getTimeFormat(TimestampUtils.DATE_FORMAT_YYY_MM_DD, System.currentTimeMillis()), mAdapter.itemCount.toString())

    private fun showLoading() {
        fragment_schedule_selector_progressbar.visibility = View.VISIBLE
        mIsLoading = true
    }

    private fun hideLoading() {
        fragment_schedule_selector_progressbar.visibility = View.GONE
        mIsLoading = false
    }

    override fun onPause() {
        super.onPause()
        mPresenter.clearObservables()
    }

    override fun onDestroy() {
        super.onDestroy()
        mPresenter.clearObservables()
    }
}