package com.ilizarraga.safebodatest.presentation.ui.adapter

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.ilizarraga.safebodatest.domain.entity.ScheduleEntity
import com.ilizarraga.safebodatest.presentation.ui.adapter.holder.ScheduleViewHolder
import com.ilizarraga.safebodatest.presentation.ui.listener.OnScheduleCellClickListener
import com.ilizarraga.safebodatest.presentation.ui.view.ScheduleItemView

class ScheduleListAdapter(private val onScheduleCellClickListener: OnScheduleCellClickListener) : RecyclerView.Adapter<ScheduleViewHolder>() {

    private var mScheduleEntityList: ArrayList<ScheduleEntity> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ScheduleViewHolder {
        return ScheduleViewHolder(ScheduleItemView(parent.context), onScheduleCellClickListener)
    }

    override fun onBindViewHolder(holder: ScheduleViewHolder, position: Int) = holder.decorate(mScheduleEntityList[position])

    override fun getItemCount(): Int = mScheduleEntityList.size

    fun addScheduleEntityList (scheduleEntityList: List<ScheduleEntity>) : List<ScheduleEntity> {
        for (scheduleEntity: ScheduleEntity in scheduleEntityList) {
            mScheduleEntityList.add(scheduleEntity)
            notifyItemInserted(itemCount - 1)
        }

        return scheduleEntityList
    }
}
