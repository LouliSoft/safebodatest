package com.ilizarraga.safebodatest.presentation.ui.adapter

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.ilizarraga.safebodatest.domain.entity.AirportEntity
import com.ilizarraga.safebodatest.presentation.ui.adapter.holder.AirportViewHolder
import com.ilizarraga.safebodatest.presentation.ui.listener.OnAirportCellClickListener
import com.ilizarraga.safebodatest.presentation.ui.view.AirportItemView

class AirportListAdapter(private val onAirportCellClickListener: OnAirportCellClickListener) : RecyclerView.Adapter<AirportViewHolder>() {

    private var mAirportEntityList: ArrayList<AirportEntity> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AirportViewHolder {
        return AirportViewHolder(AirportItemView(parent.context), onAirportCellClickListener)
    }

    override fun onBindViewHolder(holder: AirportViewHolder, position: Int) = holder.decorate(mAirportEntityList[position])

    override fun getItemCount(): Int = mAirportEntityList.size

    fun addAirportEntityList (airportEntityList: List<AirportEntity>) : List<AirportEntity> {
        for (airportEntity: AirportEntity in airportEntityList) {
            mAirportEntityList.add(airportEntity)
            notifyItemInserted(itemCount - 1)
        }

        return airportEntityList
    }
}
