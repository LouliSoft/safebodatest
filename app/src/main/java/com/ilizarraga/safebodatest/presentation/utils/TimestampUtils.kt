package com.ilizarraga.safebodatest.presentation.utils

import java.text.SimpleDateFormat
import java.util.*


class TimestampUtils {

    companion object {

        const val DATE_FORMAT_YYY_MM_DD = "yyy-MM-dd"

        fun getTimeFormat(format: String, timestamp: Long): String {

            val formatter = SimpleDateFormat(format, Locale.getDefault())

            val calendar: Calendar = Calendar.getInstance()
            calendar.timeInMillis = timestamp
            return formatter.format(calendar.time)
        }

    }
}