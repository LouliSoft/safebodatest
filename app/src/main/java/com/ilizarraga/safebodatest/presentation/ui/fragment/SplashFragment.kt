package com.ilizarraga.safebodatest.presentation.ui.fragment

import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ilizarraga.safebodatest.R
import com.ilizarraga.safebodatest.presentation.presenter.SplashPresenter
import com.ilizarraga.safebodatest.presentation.presenter.contract.SplashContract
import com.ilizarraga.safebodatest.presentation.ui.activity.AirportSelectorActivity

class SplashFragment : BaseFragment(), SplashContract.View {

    companion object {

        const val SPLASH_SCREEN_DELAY: Long = 1500

        fun newInstance() : SplashFragment {
            return SplashFragment()
        }
    }

    override var mPresenter: SplashContract.Presenter = SplashPresenter(this)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_splash, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        Handler().postDelayed({
            startActivity(AirportSelectorActivity.makeIntent(context))
            activity?.finish()
        }, SPLASH_SCREEN_DELAY)

        mPresenter.start()
    }
}