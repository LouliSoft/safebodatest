package com.ilizarraga.safebodatest.presentation.ui.adapter.holder

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import com.ilizarraga.safebodatest.domain.entity.AirportEntity
import com.ilizarraga.safebodatest.presentation.ui.listener.OnAirportCellClickListener
import com.ilizarraga.safebodatest.presentation.ui.view.AirportItemView



class AirportViewHolder(private val airportItemView: AirportItemView, private val onAirportCellClickListener: OnAirportCellClickListener) : RecyclerView.ViewHolder(airportItemView) {

    fun decorate(airportEntity: AirportEntity) {
        airportItemView.startAnimation(AnimationUtils.loadAnimation(airportItemView.context, android.R.anim.slide_in_left))
        airportItemView.layoutParams = RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        airportItemView.decorate(airportEntity)
        airportItemView.setOnClickListener { onAirportCellClickListener.onCellClick(airportEntity) }
    }
}