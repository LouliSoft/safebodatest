package com.ilizarraga.safebodatest.presentation.presenter.contract

import com.ilizarraga.safebodatest.domain.entity.ScheduleEntity
import com.ilizarraga.safebodatest.presentation.presenter.BasePresenter
import com.ilizarraga.safebodatest.presentation.presenter.BaseView

interface ScheduleSelectorContract {

    interface View : BaseView<Presenter> {

        fun showScheduleEntityList(scheduleEntityList: List<ScheduleEntity>)

        fun showTimeout()

        fun showError(error: Throwable)

    }

    interface Presenter : BasePresenter {

        fun getScheduleEntityList(origin: String, destination: String, fromDateTime: String, offset: String)

    }
}