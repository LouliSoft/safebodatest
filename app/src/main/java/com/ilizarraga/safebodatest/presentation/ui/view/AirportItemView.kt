package com.ilizarraga.safebodatest.presentation.ui.view

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.util.AttributeSet
import com.ilizarraga.safebodatest.R
import com.ilizarraga.safebodatest.domain.entity.AirportEntity
import kotlinx.android.synthetic.main.view_holder_airport.view.*

class AirportItemView : ConstraintLayout {

    constructor(context: Context) : super(context) {
        initialize()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        initialize()
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        initialize()
    }

    private fun initialize() {
        inflate(context, R.layout.view_holder_airport, this)
    }

    fun decorate(airportEntity: AirportEntity) {
        view_holder_airport_country_code_text.setText(R.string.country_code)
        view_holder_airport_country_code_text_desc.text = airportEntity.countryCode
        view_holder_airport_city_code_text.setText(R.string.city_code)
        view_holder_airport_city_code_text_desc.text = airportEntity.cityCode
        view_holder_airport_name_text.setText(R.string.name)
        view_holder_airport_name_text_desc.text = airportEntity.name
    }
}