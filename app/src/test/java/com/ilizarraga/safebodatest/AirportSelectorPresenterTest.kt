package com.ilizarraga.safebodatest

import com.ilizarraga.safebodatest.domain.entity.AirportEntity
import com.ilizarraga.safebodatest.domain.repository.AirportsRepository
import com.ilizarraga.safebodatest.presentation.presenter.AirportSelectorPresenter
import com.ilizarraga.safebodatest.presentation.presenter.contract.AirportSelectorContract
import io.reactivex.Single
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.ArgumentMatchers
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import java.util.*


class AirportSelectorPresenterTest {

    @get:Rule
    val rule = TrampolineSchedulerRule()

    @Mock
    private lateinit var view: AirportSelectorContract.View

    @Mock
    private lateinit var airportsRepository: AirportsRepository

    private lateinit var presenter: AirportSelectorPresenter

    companion object {
        const val OFFSET = "0"
        const val LANG = "ES"
        const val LHOPERATED = false
    }

    @Before
    fun setupPresenter() {
        MockitoAnnotations.initMocks(this)

        presenter = AirportSelectorPresenter(view, airportsRepository)
    }

    @Test
    fun getAirportEntityList() {

        val airportEntityList = listOf(
                AirportEntity("AAL", "AAL", "DK", "Aalborg", 56.30388889, 10.62),
                AirportEntity("ABE", "ABE", "US", "Allentown/Bethl", 40.65472222, -75.43833333),
                AirportEntity("ABK", "ABK", "ET", "Kabri Dehar", 35.04166667, -106.6063889)
        )

        Mockito.`when`(airportsRepository.getAirportsList(OFFSET, LANG, LHOPERATED))
                .thenReturn(Single.just(airportEntityList))
        presenter.getAirportEntityList(OFFSET)
        Mockito.verify(view, Mockito.never()).showAirportEntityList(airportEntityList)
        Mockito.verify(view, Mockito.never()).showError(Throwable())
    }

    @Test
    fun getAirportEntityEmptyList() {
        Mockito.`when`(airportsRepository.getAirportsList(OFFSET, LANG, LHOPERATED))
                .thenReturn(Single.just(Collections.emptyList<AirportEntity>()))
        presenter.getAirportEntityList(OFFSET)
        Mockito.verify(view, Mockito.times(1)).showAirportEntityList(ArgumentMatchers.anyList<AirportEntity>())
        Mockito.verify(view, Mockito.never()).showError(Throwable())
    }

    @Test
    fun getAirportEntityFails() {
        Mockito.`when`(airportsRepository.getAirportsList(OFFSET, LANG, LHOPERATED))
                .thenReturn(Single.error(RuntimeException()))
        presenter.getAirportEntityList(OFFSET)
        Mockito.verify(view, Mockito.never()).showAirportEntityList(ArgumentMatchers.anyList<AirportEntity>())
        Mockito.verify(view, Mockito.never()).showError(Throwable())
    }
}