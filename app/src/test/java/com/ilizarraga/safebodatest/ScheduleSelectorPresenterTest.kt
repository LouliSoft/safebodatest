package com.ilizarraga.safebodatest

import com.ilizarraga.safebodatest.domain.entity.FlightEntity
import com.ilizarraga.safebodatest.domain.entity.ScheduleEntity
import com.ilizarraga.safebodatest.domain.repository.ScheduleRepository
import com.ilizarraga.safebodatest.presentation.presenter.ScheduleSelectorPresenter
import com.ilizarraga.safebodatest.presentation.presenter.contract.ScheduleSelectorContract
import io.reactivex.Single
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.ArgumentMatchers
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import java.util.*


class ScheduleSelectorPresenterTest {

    @get:Rule
    val rule = TrampolineSchedulerRule()

    @Mock
    private lateinit var view: ScheduleSelectorContract.View

    @Mock
    private lateinit var schedulesRepository: ScheduleRepository

    private lateinit var presenter: ScheduleSelectorPresenter

    companion object {
        const val ORIGIN = "AAL"
        const val DESTINATION = "AAL"
        const val FROMDATETIME = "2018-08-09"
        const val OFFSET = "0"
        const val DIRECTFLIGHTS = false
    }

    @Before
    fun setupPresenter() {
        MockitoAnnotations.initMocks(this)

        presenter = ScheduleSelectorPresenter(view, schedulesRepository)
    }

    @Test
    fun getScheduleEntityList() {
        val scheduleEntityList = listOf(
                ScheduleEntity(listOf(
                        FlightEntity("AAL", "AAL", "LH5771"),
                        FlightEntity("AAL", "AAL", "LH5771"))),
                ScheduleEntity(listOf(
                        FlightEntity("AAL", "AAL", "LH5771"),
                        FlightEntity("AAL", "AAL", "LH5771"))),
                ScheduleEntity(listOf(
                        FlightEntity("AAL", "AAL", "LH5771"),
                        FlightEntity("AAL", "AAL", "LH5771"))))
        OFFSET
        Mockito.`when`(schedulesRepository.getScheduleList(ORIGIN, DESTINATION, FROMDATETIME, OFFSET, DIRECTFLIGHTS))
                .thenReturn(Single.just(scheduleEntityList))
        presenter.getScheduleEntityList(ORIGIN, DESTINATION, FROMDATETIME, OFFSET)
        Mockito.verify(view, Mockito.never()).showScheduleEntityList(scheduleEntityList)
        Mockito.verify(view, Mockito.never()).showError(Throwable())
    }

    @Test
    fun getScheduleEntityEmptyList() {
        Mockito.`when`(schedulesRepository.getScheduleList(ORIGIN, DESTINATION, FROMDATETIME, OFFSET, DIRECTFLIGHTS))
                .thenReturn(Single.just(Collections.emptyList<ScheduleEntity>()))
        presenter.getScheduleEntityList(ORIGIN, DESTINATION, FROMDATETIME, OFFSET)
        Mockito.verify(view, Mockito.times(1)).showScheduleEntityList(ArgumentMatchers.anyList<ScheduleEntity>())
        Mockito.verify(view, Mockito.never()).showError(Throwable())
    }

    @Test
    fun getScheduleEntityFails() {
        Mockito.`when`(schedulesRepository.getScheduleList(ORIGIN, DESTINATION, FROMDATETIME, OFFSET, DIRECTFLIGHTS))
                .thenReturn(Single.error(RuntimeException()))
        presenter.getScheduleEntityList(ORIGIN, DESTINATION, FROMDATETIME, OFFSET)
        Mockito.verify(view, Mockito.never()).showScheduleEntityList(ArgumentMatchers.anyList<ScheduleEntity>())
        Mockito.verify(view, Mockito.never()).showError(Throwable())
    }
}